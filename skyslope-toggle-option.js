import { LitElement, html } from '@polymer/lit-element';

class SkySlopeToggleOption extends LitElement {
  static get properties() {
    return {
      id: {type: String},
      value: {type: String},
      checked: {type: Boolean, reflect: true},
      disabled: {type: Boolean, reflect: true},
    };
  }

  constructor() {
    super();
    this.value = '';
    this.checked = false;
    this.disabled = false;
    this.addEventListener('click', this.handleClick);
  }

  handleClick (e) {
    if (this.disabled) {
      e.preventDefault();
      e.stopPropagation();
      return false;
    }
    this.checked = !this.checked;
    this.dispatchEvent(
      new CustomEvent('change', {
        bubbles: true,
        composed: true,
        detail: {
          checked: this.checked,
          value: this.value,
        },
      })
    );
  }

  defaultStyles() {
    return html`
      <style>
        :host([hidden]) {
          display: none;
        }

        :host {
          display: inline-flex;
        }

        :host(:first-child) {
          --skyslope-toggle-option-border-radius: 3px 0px 0px 3px;
        }

        :host(:last-child) {
          --skyslope-toggle-option-border-radius: 0px 3px 3px 0px;
        }

        :host(:only-child) {
          --skyslope-toggle-option-border-radius: 3px;
        }

        .option-item {
          border: 1px solid #6B6B6B;
          border-radius: var(--skyslope-toggle-option-border-radius, 1px);
          background-color: #FFFFFF;
          margin: -1;
          min-width: 96px;
          min-height: 48px;
          font-family: 'Open Sans', Arial, Helvetica, sans-serif;
          font-size: 16px;
          font-weight: 600;
          display: inline-flex; 
          justify-content: center;
          align-items: center;
          box-sizing: border-box;
          color: #6B6B6B;
          text-transform: uppercase;
          cursor: pointer;
          padding: 10px;
        }

        .option-item[checked] {
          background-color: #1070C3;
          border-color: #1070C3;
          color: #FFFFFF;
          box-shadow: inset 0px 0px 2px #6B6B6B;
        }

        .option-item:hover {  
          background-color: #EEEEEE;
          color: #6B6B6B;
        }

        .option-item[checked]:hover {
          background-color: #1070C3;
          border-color: #1070C3;
          color: #FFFFFF;
          box-shadow: inset 0px 0px 2px #6B6B6B;
        }

        .option-item:disabled {  
          cursor: default;
        }

        .option-item:focus {
          outline-offset: -4px;
        }
      </style>
    `
  }

  render() {
    return html`
      ${this.defaultStyles()}
      <button class="option-item" ?checked=${this.checked} ?disabled=${this.disabled}>
        <slot></slot>
      </button>
    `;
  }
}

customElements.define('skyslope-toggle-option', SkySlopeToggleOption);
